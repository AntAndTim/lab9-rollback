package me.antandtim.rollback

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RollbackApplication

fun main(args: Array<String>) {
    runApplication<RollbackApplication>(*args)
}
