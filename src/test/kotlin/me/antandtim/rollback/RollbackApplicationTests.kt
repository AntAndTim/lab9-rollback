package me.antandtim.rollback

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import javax.sql.DataSource

@SpringBootTest
class RollbackApplicationTests {
    
    @Autowired
    private lateinit var dataSource: DataSource
    
    @Test
    fun `Ща потестим наше говно`() {
        var conn: Connection? = null
        var stmt: Statement? = null
        try {
            println("Connecting to database...")
            conn = dataSource.connection
            conn.autoCommit = false
            
            println("Creating statement...")
            stmt = conn.createStatement(
                ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE
            )
            
            println("Inserting one row....")
            var SQL = "INSERT INTO Employees VALUES (108, 20, 'Rita', 'Tez')"
            stmt.executeUpdate(SQL)
            
            println("Inserting one row....")
            SQL = "INSERT INTO Employees VALUES (106, 20, 'Rita', 'Tez')"
            stmt.executeUpdate(SQL)
            
            SQL = "INSERT INTO Employees VALUES (107, 22, 'Sita', 'Singh')"
            stmt.executeUpdate(SQL)
            
            // Commit data here.
            println("Commiting data here....")
            conn.commit()
            
            val sql = "SELECT id, first, last, age FROM Employees"
            val rs = stmt.executeQuery(sql)
            
            println("List result set for reference....")
            printRs(rs)
            
            addSalaries(stmt, listOf(100, 100), listOf(108, 1337), listOf(1, 33))
            
            rs.close()
            stmt.close()
            conn.close()
        } catch (se: SQLException) {
            //Handle errors for JDBC
            se.printStackTrace()
            // If there is an error then rollback the changes.
            println("Rolling back data here....")
            try {
                conn?.rollback()
            } catch (se2: SQLException) {
                se2.printStackTrace()
            } //end try
        } catch (e: Exception) {
            //Handle errors for Class.forName
            e.printStackTrace()
        } finally {
            //finally block used to close resources
            try {
                stmt?.close()
            } catch (se2: SQLException) {
            }
            try {
                conn?.close()
            } catch (se: SQLException) {
                se.printStackTrace()
            } //end finally try
        }
        println("Goodbye!")
    }
    
    fun printRs(rs: ResultSet) {
        //Ensure we start with first row
        rs.beforeFirst()
        while (rs.next()) {
            //Retrieve by column name
            val id = rs.getInt("id")
            val age = rs.getInt("age")
            val first = rs.getString("first")
            val last = rs.getString("last")
            
            //Display values
            print("ID: $id")
            print(", Age: $age")
            print(", First: $first")
            println(", Last: $last")
        }
        println()
    }
    
    fun addSalaries(stmt: Statement, ids: List<Int>, employeeIds: List<Int>, amount: List<Int>) {
        ids.forEach {
            try {
                stmt.connection.prepareStatement(
                    "INSERT INTO Salary " +
                        "VALUES (" + ids[it].toString() + "," +
                        employeeIds[it].toString() + "," +
                        amount[it].toString() + ")"
                ).executeUpdate()
                stmt.connection.commit()
            } finally {
                stmt.connection.rollback()
            }
        }
    }
}
